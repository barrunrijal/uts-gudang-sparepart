package novan.seka.sparepart

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context : Context):SQLiteOpenHelper(context,DB_Name, null,DB_Ver) {

    companion object {
        val DB_Name = "sparepart"
        val DB_Ver = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val tJenis = "create table jenis(id_jenis integer primary key autoincrement, nama_jenis text not null)"
        val tBrg = "create table brg(kode text primary key, nama_brg text not null, stok text not null, id_jenis Int not null)"
        val insJenis = "insert into jenis(nama_jenis) values('SparePart'),('Oli'),('Variasi'),('Accesoris')"
        db?.execSQL(tBrg)
        db?.execSQL(tJenis)
        db?.execSQL(insJenis)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }
}